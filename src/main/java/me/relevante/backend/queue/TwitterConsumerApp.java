package me.relevante.backend.queue;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@EnableRabbit
@EnableAutoConfiguration
@ComponentScan("me.relevante")
@EntityScan("me.relevante")
public class TwitterConsumerApp {
	
	public static void main(String[] args) {
		SpringApplication.run(TwitterConsumerApp.class, args);
	}
}