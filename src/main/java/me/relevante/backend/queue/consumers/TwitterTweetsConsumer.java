package me.relevante.backend.queue.consumers;

import me.relevante.backend.queue.QueueNames;
import me.relevante.backend.queue.persistence.TwitterShareRepo;
import me.relevante.backend.queue.persistence.TwitterUserRepo;
import me.relevante.backend.queue.requests.ProcessContactsRequest;
import me.relevante.twitter.model.TwitterShare;
import me.relevante.twitter.model.TwitterUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import twitter4j.*;
import twitter4j.auth.AccessToken;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class TwitterTweetsConsumer extends BaseTwitterConsumer {

	private Logger logger = LoggerFactory.getLogger(TwitterTweetsConsumer.class);

	@Autowired
	private TwitterShareRepo twitterShareRepo;
	@Autowired
	private TwitterUserRepo twitterUserRepo;

	@RabbitListener(queues = QueueNames.TWITTER_TWEETS)
	public void processTwitterTweets(byte[] data) throws IOException {

		ProcessContactsRequest processContactsRequest = objectMapper.readValue(data, ProcessContactsRequest.class);
		Twitter instance = twitterFactory.getInstance();
		AccessToken accessToken = new AccessToken(processContactsRequest.getToken(), processContactsRequest.getSecret());
		instance.setOAuthAccessToken(accessToken);

		ResponseList<Status> homeTimeline;
		try {
            instance.verifyCredentials();
			homeTimeline = instance.getHomeTimeline(new Paging(1, 500));
			Map<Long, TwitterUser> updatedUsers = new HashMap<>();
            for (Status tweet : homeTimeline) {
                processTweet(processContactsRequest.getRelevanteId(), tweet, updatedUsers);
            }
            twitterUserRepo.save(updatedUsers.values());
		}
		catch (TwitterException e) {
        	logger.error("Exception in queue: " + QueueNames.TWITTER_TWEETS + " processing data " + data, e);
		}
	}

    private void processTweet(Long relevanteId,
                              Status apiTweet,
                              Map<Long, TwitterUser> updatedUsers) {

        User apiUser = apiTweet.getUser();
        TwitterShare twitterShare = twitter2Twitter4j.createTwitterShareFromTwitter4jShare(apiTweet);
        TwitterUser twitterUser = twitter2Twitter4j.createTwitterUserFromTwitter4jUser(apiUser);
        TwitterUser existingUserTwitter = twitterUserRepo.findOne(apiUser.getId());

        if (existingUserTwitter != null) {
            twitterUser.setTwitterId(Long.parseLong(existingUserTwitter.getId()));
            twitterUser.setKeywords(existingUserTwitter.getKeywords());
        }
        if (!apiUser.isProtected() && apiUser.getURL() != null) {
            updatedUsers.put(twitterUser.getTwitterId(), twitterUser);
        }
        if (!twitterShareRepo.exists(twitterShare.getId())) {
            twitterShareRepo.save(twitterShare);
            System.out.println("Saving tweet " + apiTweet.getId() + " of user " + apiTweet.getUser().getId() + " contact of " + relevanteId);
        }

    }

}
