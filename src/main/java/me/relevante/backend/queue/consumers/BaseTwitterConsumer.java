package me.relevante.backend.queue.consumers;

import me.relevante.backend.queue.consumer.BaseConsumer;
import me.relevante.twitter.api.Twitter2Twitter4j;
import org.springframework.beans.factory.annotation.Autowired;
import twitter4j.TwitterFactory;

public abstract class BaseTwitterConsumer extends BaseConsumer {

	@Autowired
	protected TwitterFactory twitterFactory;
    @Autowired
    protected Twitter2Twitter4j twitter2Twitter4j;

}