
package me.relevante.backend.queue.consumers;

import me.relevante.backend.queue.QueueNames;
import me.relevante.backend.queue.persistence.RelatedUserRepo;
import me.relevante.backend.queue.persistence.TwitterUserRepo;
import me.relevante.backend.queue.requests.ProcessContactsRequest;
import me.relevante.model.Network;
import me.relevante.model.RelatedUser;
import me.relevante.twitter.model.TwitterUser;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import twitter4j.PagableResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.auth.AccessToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class TwitterContactsConsumer extends BaseTwitterConsumer {

	@Autowired
	private TwitterUserRepo twitterUserRepo;
	@Autowired
	private RelatedUserRepo relatedUserRepo;

	@RabbitListener(queues = QueueNames.TWITTER_CONTACTS)
	public void processTwitterContacts(byte[] data) throws IOException, TwitterException {

		ProcessContactsRequest processContactsRequest = objectMapper.readValue(data, ProcessContactsRequest.class);
		Twitter instance = twitterFactory.getInstance(new AccessToken(processContactsRequest.getToken(), processContactsRequest.getSecret()));

		List<TwitterUser> users = new ArrayList<>();
		List<RelatedUser> usersContact = new ArrayList<>();
		
		long cursor = -1;
		PagableResponseList<User> friendsList;
		do {
			friendsList = instance.getFriendsList(instance.verifyCredentials().getId(), cursor, 200);
			Iterator<User> iterator = friendsList.iterator();
			while (iterator.hasNext()) {
				User user = iterator.next();
                TwitterUser newTwitterUser = twitter2Twitter4j.createTwitterUserFromTwitter4jUser(user);
                TwitterUser existingUserTwitter = twitterUserRepo.findOne(user.getId());
				if (existingUserTwitter != null) {
					newTwitterUser.setTwitterId(Long.parseLong(existingUserTwitter.getId()));
					newTwitterUser.setKeywords(existingUserTwitter.getKeywords());
				}
				if (!user.isProtected() && user.getURL() != null) {
					users.add(newTwitterUser);
					usersContact.add(new RelatedUser(processContactsRequest.getRelevanteId(), String.valueOf(user.getId()), Network.TWITTER, true));
				}
			}
			cursor = friendsList.getNextCursor();
		}
		while (friendsList.hasNext());

        twitterUserRepo.save(users);
		relatedUserRepo.save(usersContact);
	}
}