package me.relevante.backend.queue.persistence;

import me.relevante.twitter.model.TwitterUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterUserRepo extends CrudRepository<TwitterUser, Long> {
}
