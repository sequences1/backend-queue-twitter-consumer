package me.relevante.backend.queue.persistence;

import me.relevante.model.RelatedUser;
import me.relevante.model.RelatedUserId;
import org.springframework.data.repository.CrudRepository;

public interface RelatedUserRepo extends CrudRepository<RelatedUser, RelatedUserId> {
}
