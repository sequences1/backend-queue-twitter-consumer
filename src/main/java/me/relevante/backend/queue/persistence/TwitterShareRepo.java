package me.relevante.backend.queue.persistence;

import me.relevante.twitter.model.TwitterShare;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterShareRepo extends CrudRepository<TwitterShare,String> {
}
