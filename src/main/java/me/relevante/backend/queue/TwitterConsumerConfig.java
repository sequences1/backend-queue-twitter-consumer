package me.relevante.backend.queue;

import me.relevante.backend.queue.consumer.BaseConsumerConfig;
import me.relevante.model.oauth.OAuthKeyPair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

@Configuration
public class TwitterConsumerConfig extends BaseConsumerConfig {

    @Value("${twitter.apiKey}")
    private String apiConsumerKey;
    @Value("${twitter.apiSecret}")
    private String apiConsumerSecret;

    @Bean
    public TwitterFactory twitterFactory() {
        TwitterFactory twitterFactory = new TwitterFactory(new ConfigurationBuilder().setOAuthConsumerKey(apiConsumerKey).setOAuthConsumerSecret(apiConsumerSecret).build());
        return twitterFactory;
    }

    @Bean
    public OAuthKeyPair twitterConsumerKeyPair() {
        OAuthKeyPair oAuthKeyPair = new OAuthKeyPair(apiConsumerKey, apiConsumerSecret);
        return oAuthKeyPair;
    }

}
